 require 'sinatra'
 require 'data_mapper'
 
 configure do
 # DB Connection Info
 DataMapper.setup(:default, 'postgres://localhost/development')
 DataMapper.setup(:default, {
  :adapter  => 'postgres',
  :host     => '127.0.0.1',
  :username => 'sysdba' ,
  :password => 's3cret',
  :database => 'development'
 })
 end
  # Adding Models For Tables in Databases
 class Linea
  include DataMapper::Resource
  property :cve_linea, String, :length => 4, :required => true, :key => true
  property :linea, String, :length => 30, :required => true
 end
 #sets the data for the database attributes
 DataMapper.auto_upgrade!
 
 def campo_vacio(campo)
    campo.empty?
 end
 #this will allow the app to check if the user leaves any required field empty
 
 # main view
 get '/inicio' do #this will allow the user to set an specific path to the main view
  erb :index #if if i write here /, then no additional routing is defined
 end
 
 # Shows error view if the user leaves an empty required field
 get '/campo_vacio' do
  erb :error_campovacio
 end
  #Sends The User to the adding a new "linea" view
 get '/linea' do
  erb :reg_linea
 end
 
 # Display results
 get '/listalineas' do
  @lineas = Linea.all :order => :cve_linea
  erb :lista_lineas
 end
 
 #Saving a "linea"
 post '/linea' do
 @linea = Linea.new(:cve_linea => params[:cve_linea].upcase, :linea => params[:linea].upcase)
  if !campo_vacio(params[:cve_linea]) and !campo_vacio(params[:linea])
     if @linea.save
        redirect "/linea/#{@linea.cve_linea}"
     else
         redirect '/linea'
     end
  else
      redirect '/campo_vacio'
   end
 end

 # Displays selected linea details
 get '/linea/:cve_linea' do
  @linea = Linea.get(params[:cve_linea])
  if @linea
     erb :cons_linea
  else
      redirect '/linea'
  end
 end

 # Displays Edit View
 get '/editlinea/:cve_linea' do
  @linea = Linea.get(params[:cve_linea])
  if @linea
     erb :edit_linea
  else
      redirect '/listalineas'
  end
 end

#Shows form to delete an entry
 get '/borralinea/:cve_linea' do
  @linea = Linea.get(params[:cve_linea])
  if @linea
     erb :borra_linea
  else
      redirect '/listalineas'
  end
 end

 # Allows the user to edit the "lineas"
 put '/editlinea/:cve_linea' do
  @linea = Linea.get(params[:cve_linea])
  if @linea
     @linea.linea = params[:linea].upcase
     @linea.save
     redirect '/listalineas'
  end
 end

  # borra la linea
 delete '/borralinea/:cve_linea' do
  @linea = Linea.get(params[:cve_linea])
  if @linea
     @linea.destroy
     redirect '/listalineas'
   end
 end
 $end 
 